<!--
.. title: Accessibility validation of user interfaces through continuous integration
.. slug: idea-accessibility-validation-continuous-integration
.. author: Christian Frisson
.. date: 2023-02-02 14:23:11 UTC-05:00
.. tags: ideas, easy, 175 hours, Poire, Satellite, Scenic, Splash
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Accessibility validation of user interfaces through continuous integration

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

Quoting a quote from [https://en.wikipedia.org/wiki/Accessibility](https://en.wikipedia.org/wiki/Accessibility) :
> Accessibility is the design of products, devices, services, vehicles, or environments so as to be usable by people with disabilities.

The goal of this project idea is to implement validation tests during continuous integration with [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) so that we ensure that user interfaces developed at the SAT consistently meet accessibility standards. 

This project idea is flexible on the identification and amount of SAT tools to be covered. 
Please check this a [list of SAT tools](https://sat-mtl.gitlab.io), including the following tools that feature user interfaces:

| SAT Tools                                                          | User interface toolkit                    |
| ------------------------------------------------------------------ | ----------------------------------------- |
| [Poire](https://gitlab.com/sat-mtl/metalab/poire)                  | [React](https://reactjs.org/)             |
| [Satellite](https://hub.satellite.sat.qc.ca)                       | [React](https://reactjs.org/)             |
| [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic)           | [React](https://reactjs.org/)             |
| [Splash](https://gitlab.com/sat-mtl/tools/splash/splash)           | [ImGui](https://github.com/ocornut/imgui) |
| [Telluriq](https://gitlab.com/sat-mtl/tools/haptic-floor/telluriq) | [Kivy](https://kivy.org/)                 |

Note that some of our tools reuse the same front-end stacks. 
For instance, [Poire](https://gitlab.com/sat-mtl/metalab/poire) and [Satellite](https://hub.satellite.sat.qc.ca) (based on [Mozilla Hubs](https://github.com/mozilla/hubs/)) and [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic) both rely on [React](https://reactjs.org/), whose [documentation](https://reactjs.org/docs/accessibility.html) provides best practices for accessibility with this toolkit, including using the [eslint-plugin-jsx-a11y](https://github.com/evcohen/eslint-plugin-jsx-a11y) plugin for AST linting feedback regarding accessibility issues in JSX files, so it would make sense to submit a proposal for this project idea that would cover more than one SAT tool.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Implement validation tests in continuous integration with [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) for a selection of one or more SAT tool(s) with user interfaces.
* Apply fixes to improve accessibility based on validation test and a review of accessibility guidelines.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* preferred: experience with or willingness to learn development languages and front-end stacks of the selected SAT tools UIs (for instance JavaScript and React for [Poire](https://gitlab.com/sat-mtl/metalab/poire) and [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic) and [Satellite](https://hub.satellite.sat.qc.ca))
* preferred: experience with or willingness to learn [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Christian Frisson](https://gitlab.com/christianfrisson), [Olivier Gauthier](https://gitlab.com/ogauthier_sat), [Guillaume Riou](https://gitlab.com/guillaumeriousat)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

175 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

easy
