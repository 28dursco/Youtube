<!--
.. title: SATIE4Blender UX improvements
.. slug: idea-satie4blender-ux-improvements
.. author: Michał Seta
.. date: 2023-02-06 16:01:11 UTC-05:00
.. tags: ideas, medium, 350 hours, Blender, Python, SATIE
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!--
Please update post metadata:
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT)
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

SATIE4Blender UX improvements

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment -->

[SATIE4Blender](https://gitlab.com/sat-mtl/tools/satie/satie4blender) is a addon for [Blender](https://www.blender.org/) to allow interactions with [SATIE](https://gitlab.com/sat-metalab/SATIE), our audio spatialization engine.
Its aim is to provide one possible way to prototype 3D scenes with spatial (i.e. multi-speaker), immersive audio. SATIE and its software ecosystem have undergone some changes recently and the Blender addon needs some adjustments and, especially, improvements from the point of view of user experience.

The GSoC contributor will work off the existing code base to improve the existing integration with Blender and interactions with SATIE. The project is at this moment at a working prototype level, demonstrating the usefulness and basic functionality.

## Expected outcomes

<!-- Please add 2-5 items below this comment -->

* A Blender addon that provides the following functionality:
  * Installs easily on all major platforms
  * Provides a consistent user experience
  * Integrates well with Blender UI and UX
* (Bonus) Present our joint contributions at an international conference

## Skills required/preferred

<!-- Please add 2-5 items below this comment -->

* required: experience with Blender
* required: intermediate knowledge of Python
* preferred: experience with Blender's API for Python
* preferred: understanding of the fundamentals of digital arts technologies
* preferred: interest in audio-visual technologies
* preferred: willingness to learn some fundamentals of [SuperCollider](https://supercollider.github.io/), upon which SATIE is built

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Michał Seta](https://gitlab.com/djiamnot), [Edu Meneses](https://gitlab.com/edumeneses)

## Expected size of project

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty

<!-- Please write below this comment either: easy, medium or hard -->

medium
