---
title: Work Product for Create a WebUI for data mapping and software control in embedded systems
slug: work-product-tiger-yash
author: Yash Raj
date: 2022-09-05 12:00:00 UTC+05:30
tags: contributions, products, LivePose, SATIE, Poire
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Yash Raj
* website: website: [https://tiger-yash.github.io/](https://tiger-yash.github.io/)
* gitlab username: [@tiger-yash](https://gitlab.com/tiger-yash)
* timezone: UTC+5:30

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->

Create a WebUI for data mapping and software control in embedded systems

## Short description of work done

<!-- 
Please write a short description of work done. 
(150-200 words)
-->

 Most of my work during the Google Summer of Code 2022 project focused on integrating [LivePose](https://gitlab.com/sat-metalab/livepose) into [Poire](https://gitlab.com/sat-metalab/poire), refining the interface, updating and improving the functionality of the already integrated tool [SATIE](https://gitlab.com/sat-metalab/satie), and making them accessible and configurable for the users. 

The WebUI can start or stop the tools (LivePose and SATIE), control Jack Audio parameters, and request a reboot/service restart). 
In other words, Poire can remotely launch two SAT tools in embedded systems based on a generic computer running Linux. 
Poire can also be used to set desired configurations for both LivePose and SATIE, which can be launched and managed simultaneously using the WebUI.

LivePose can be launched with three different backend options, with a specialized set of configurations for each backend. 
The backend for LivePose has been implemented in such a way that LivePose automatically reboots on changing some configurations with the user not having to terminate it before any changes manually. 
The configuration settings for both the tools and all the current Digital Playgrounds Node data for SATIE are fetched and repopulated in the UI on reset.

## What code got merged

<!-- 
Please list all merge requests that got merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

* [Add LivePose control in Poire](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/44)
    * **Description**
        * Modified Poire UI to include LivePose and its settings.
        * Added the backend options for LivePose and their respective configuration settings.
        * Made LivePose easy to launch as well as terminate in the backend server.
        * Implemented threading to support both tools to be run and managed simultaneously.
        * Made data persist on refresh by storing, fetching and repopulating data from the backend server, for both SATIE and LivePose.
        * Repopulating all SATIE nodes created on reset, with no loss in data.
    * **Major Commits**
        * [LivePose added to Poire](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/44/diffs?commit_id=25025dfd754c951b991df928db41a6c048532880)
        * [LivePose pose_backend configurations added](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/44/diffs?commit_id=3d0d3613955a33022ade82fee0a791419631ac49)
        * [fetch Satie config on reload fixed](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/44/diffs?commit_id=84fae0ad9ec1c4191d8e23c2b69a4ec0092dc9dd)
        * [SATIE Source Nodes data fetching fixed](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/44/diffs?commit_id=8d84c03c513541d356f942719725b4c02d4f1da8)
        * [Synthdefs persistence added](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/44/diffs?commit_id=2056a32f69f964245ce8f6e9894a1e86d2d19adb)
        * [documentation update](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/44/diffs?commit_id=10bbb7519373d3eaa3d461c2f1dd09eef9b9cba1)

## What code didn’t get merged

<!-- 
Please list all merge requests that did not get merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

* [Satie Spatializers list update](https://gitlab.com/sat-mtl/metalab/poire/-/merge_requests/49)

## What’s left to do

<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->

* To create a proper JSON schema for configuration with all details for all LivePose configurations.
* To make the configuration fields of LivePose settings dynamic in the frontend, using the configuration schema.
* To work on the availability of all Listening Formats for SATIE without booting it.

